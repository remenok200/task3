
var button = document.querySelector('#start-button');
var output = document.querySelector('#output');

button.addEventListener('click', function () {
  // Create a new Promise here and use setTimeout inside the function you pass to the constructor

  setTimeout(function () { // <- Store this INSIDE the Promise you created!
    // Resolve the following URL: https://swapi.co/api/people/1


    const fetchPromise = fetch("https://swapi.dev/api/people/1/");
    fetchPromise.then(response => {
      return response.json(); // Превращаем ответ в JSON обьект
    }).then(response => {
      output.innerHTML += ' ' + response.name; //Выводим данные на страницу
    });



  }, 3000);

  // Handle the Promise "response" (=> the value you resolved) and return a fetch()
  // call to the value (= URL) you resolved (use a GET request)

  // Handle the response of the fetch() call and extract the JSON data, return that
  // and handle it in yet another then() block

  // Finally, output the "name" property of the data you got back (e.g. data.name) inside
  // the "output" element (see variables at top of the file)

  someData = { person: { name: 'Max', age: 28 } };
  const putMethod = {
    method: 'PUT', // Method itself
    headers: {
      'Content-type': 'application/json; charset=UTF-8' // Indicates the content 
    },
    body: JSON.stringify(someData) // We send data in JSON format
  }

  // make the HTTP put request using fetch api
  fetch('https://httpbin.org/put', putMethod)
    .then(response => response.json())
    .then(data => output.innerHTML += ' ' + JSON.parse(data.data).person.name)
    .catch(err => console.log(err)) // Обработка ошибок
  // Repeat the exercise with a PUT request you send to https://httpbin.org/put
  // Make sure to set the appropriate headers 
  // Send any data of your choice, make sure to access it correctly when outputting it
  // Example: If you send {person: {name: 'Max', age: 28}}, you access data.json.person.name
  // to output the name (assuming your parsed JSON is stored in "data")

  // To finish the assignment, add an error to URL and add handle the error both as
  // a second argument to then() as well as via the alternative taught in the module
});